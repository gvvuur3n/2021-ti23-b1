#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_http_client.h"
#include "cJSON.h"

#include "weather_module.h"
#include "ip_module.h"
#include "wifi_module.h"

ESP_EVENT_DEFINE_BASE(WEATHER_MODULE_EVENT);

static void get_weather_task();
static esp_err_t get_weather_task_event_handler(esp_http_client_event_t *evt);
static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

static const char *TAG = "Weather module";

/**
 * 
 * When the ip has been fetched the event_handler will get the current time for the user.
 * 
 **/
static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    static TaskHandle_t weather_task_handle;

    if (event_base == IP_MODULE_EVENT && event_id == IP_DATA_FETCHED)
    {
        // get the IP event data
        ip_data_t *ip_data = (ip_data_t *)event_data;

        if (weather_task_handle == NULL)
        {
            // create weather task with lowest priority
            xTaskCreate(get_weather_task, "getWeatherTask", 4096, ip_data->city, tskIDLE_PRIORITY, &weather_task_handle);
        }
    }
    else if (event_base == WIFI_MODULE_EVENT && event_id == WIFI_DISCONNECTED)
    {
        if (weather_task_handle != NULL)
        {
            // delete weather task assuming it's still running
            vTaskDelete(weather_task_handle);
            weather_task_handle = NULL;
        }
    }
}

static esp_err_t get_weather_task_event_handler(esp_http_client_event_t *evt)
{
    // create buffer for full output message
    static char *output_buffer;
    static int output_len;

    //checks if the data has been acquired
    if (evt->event_id == HTTP_EVENT_ON_DATA)
    {
        if (!esp_http_client_is_chunked_response(evt->client))
        {
            if (output_buffer == NULL)
            {
                // initialize output buffer
                output_buffer = (char *)malloc(esp_http_client_get_content_length(evt->client));
                output_len = 0;
                if (output_buffer == NULL)
                {
                    ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
                    return ESP_FAIL;
                }
            }

            // copy new data to output buffer
            memcpy(output_buffer + output_len, evt->data, evt->data_len);
            output_len += evt->data_len;
        }
    }
    //checks if the event is finished
    else if (evt->event_id == HTTP_EVENT_ON_FINISH)
    {
        if (output_buffer != NULL)
        {
            const cJSON *main_json = NULL;
            const cJSON *temp_json = NULL;

            // parse output buffer to json object
            cJSON *root = cJSON_Parse(output_buffer);

            // fetch main object from root object
            main_json = cJSON_GetObjectItem(root, "main");
            if (!cJSON_IsObject(main_json))
            {
                ESP_LOGE(TAG, "Parsing error: main is not an object");
            }

            // fetch temp number from main object
            temp_json = cJSON_GetObjectItem(main_json, "temp");
            if (!cJSON_IsNumber(temp_json))
            {
                ESP_LOGE(TAG, "Parsing error: temp is not a number");
            }

            // create event data
            weather_data_t weather_data = {
                .temperature = temp_json->valuedouble};

            // post weather data event to the default event loop
            ESP_ERROR_CHECK(esp_event_post(WEATHER_MODULE_EVENT, IP_DATA_FETCHED, &weather_data, sizeof(weather_data_t), portMAX_DELAY));

            // free output buffer
            free(output_buffer);
            output_buffer = NULL;
        }
        output_len = 0;
    }
    //checks if the http disconnected
    else if (evt->event_id == HTTP_EVENT_DISCONNECTED)
    {
        ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
        if (output_buffer != NULL)
        {
            // free output buffer
            free(output_buffer);
            output_buffer = NULL;
        }
    }

    return ESP_OK;
}

static void get_weather_task(void *data)
{
    // convert task data to city
    char *city = (char *)data;

    while (1)
    {
        // generate url from city and api key
        char *url_buffer = (char *)malloc(snprintf(NULL, 0, "https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric", city, CONFIG_ESP_WEATHER_APIKEY));
        sprintf(url_buffer, "https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric", city, CONFIG_ESP_WEATHER_APIKEY);

        // initialize HTTP request
        esp_http_client_config_t config = {
            .url = url_buffer,
            .event_handler = get_weather_task_event_handler};

        esp_http_client_handle_t client = esp_http_client_init(&config);

        // perform HTTP request
        esp_err_t err = esp_http_client_perform(client);

        if (err == ESP_OK)
        {
            int status_code = esp_http_client_get_status_code(client);
            int content_length = esp_http_client_get_content_length(client);

            ESP_LOGI(TAG, "Status = %d, content_length = %d", status_code, content_length);
        }

        // cleanup HTTP request
        esp_http_client_cleanup(client);

        // await interval
        vTaskDelay(CONFIG_ESP_WEATHER_INTERVAL * 1000 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);
}

void init_weather_module()
{
    // register to WiFi events
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
    // register to IP events
    ESP_ERROR_CHECK(esp_event_handler_register(IP_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
}