#ifndef WEATHER_H
#define WEATHER_H

#include "esp_event.h"

/**
 * @brief Enumeration that shows if the data has been fetched or not.
 * 
 */
typedef enum {
    WEATHER_DATA_FETCHED
} weather_module_event_t;

/**
 * @struct weather_data_t
 * @brief Used to contain the data of the current weather.
 * 
 * @param temperature contains the temperature at this moment.
 * 
 */
typedef struct {
    double temperature;
} weather_data_t;

ESP_EVENT_DECLARE_BASE(WEATHER_MODULE_EVENT);

/**
 * 
 * @brief init_weather_module initialises the weather_module. It adds this module toe the EventLoop so when the 
 * program goes through the loop is also refreshes the weather.
 * 
 **/
void init_weather_module();

#endif