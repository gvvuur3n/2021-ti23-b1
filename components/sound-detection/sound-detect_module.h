//
// Created by Grant van Vuuren on 15/03/2021.
//

#ifndef TI23_B1_SOUND_DETECT_MODULE_H
#define TI23_B1_SOUND_DETECT_MODULE_H

#include "esp_err.h"
#include "esp_log.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "goertzel_filter.h"
#include "freertos/semphr.h"
#include "sdkconfig.h"
#include "esp_system.h"
#include <stdio.h>


/**
 * @brief These enums are used to define an event_id.
 * 
 * @param board_handle_param Management of sending and receiving events from the audio board.
 * @param periph_handle_param Library that simplifies management of peripherals for basic functions to send and receive events.
 */
void soundDetection_init(audio_board_handle_t board_handle_param, esp_periph_set_handle_t periph_handle_param);

#endif //TI23_B1_SOUND_DETECT_MODULE_H
