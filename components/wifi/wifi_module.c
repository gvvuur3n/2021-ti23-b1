#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>

#include "esp_wifi.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "wifi_module.h"

ESP_EVENT_DEFINE_BASE(WIFI_MODULE_EVENT);

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

static const char *TAG = "WiFi module";

/**
 * 
 * Checks if the STA started or disconnected. Also looks if the STA got the IP.
 * 
 */
static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        // WiFi station driver started
        ESP_LOGI(TAG, "STA started");
        // connect to access point
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        // WiFi station disconnected
        ESP_LOGI(TAG, "STA disconnected");
        // post WiFi disconnected event to the default event loop
        ESP_ERROR_CHECK(esp_event_post(WIFI_MODULE_EVENT, WIFI_DISCONNECTED, NULL, 0, portMAX_DELAY));
        // reconnect to access point
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        // WiFi statio got IP address from access point
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        // post WiFi connected event to the default event loop
        ESP_ERROR_CHECK(esp_event_post(WIFI_MODULE_EVENT, WIFI_CONNECTED, NULL, 0, portMAX_DELAY));
        ESP_LOGI(TAG, "Got IP:" IPSTR, IP2STR(&event->ip_info.ip));
    }
}

/**
 * 
 * init_wifi initialises the wifi.
 * 
 **/ 
void init_wifi()
{
    // disable logging for underlying code
    esp_log_level_set("wifi", ESP_LOG_WARN);
    esp_log_level_set("wifi_init", ESP_LOG_WARN);
    esp_log_level_set("phy", ESP_LOG_WARN);

    // initialize network stack
    ESP_ERROR_CHECK(esp_netif_init());

    // initialize wifi sta stack
    esp_netif_create_default_wifi_sta();

    // register to wifi and ip events
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, NULL));

    // set configuration
    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    wifi_config_t wifi_config = {.sta = {
        .ssid = CONFIG_ESP_WIFI_SSID,
        .password = CONFIG_ESP_WIFI_PASSWORD
    }};

    // initialize wifi driver
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
    // set wifi mode to STA (station, a.k.a. WiFi consumer)
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    // set wifi config
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    // start wifi driver
    ESP_ERROR_CHECK(esp_wifi_start());
}