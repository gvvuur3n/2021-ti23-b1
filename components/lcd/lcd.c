#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <sys/time.h>
#include <esp_log.h>
#include <driver/i2c.h>

#include <hd44780.h>
#include <lcd.h>

static const char TAG[] = "lcd";
static const uint8_t CHARSET[] = {
    0b00000000, 0b00001100, 0b00011110, 0b00011111, 0b00000010, 0b00001001, 0b00000100, 0b00000000, // <- Rainy cloud : GCRam position 1
    0b00000000, 0b00000110, 0b00000111, 0b00000100, 0b00000100, 0b00011100, 0b00011000, 0b00000000, // <- Music Note : GCRam position 2
    0b00000000, 0b00000000, 0b00010101, 0b00001110, 0b00011111, 0b00001110, 0b00010101, 0b00000000, // <- Sunny Sun : GCRam position 3
    0b00000000, 0b00001110, 0b00010001, 0b00000000, 0b00000100, 0b00001010, 0b00000000, 0b00000100, // <- Wifi signal : GCRam position 4
};

static lcd_info_t *global_lcd_info;

static esp_err_t _write_lcd_data(const hd44780_t *lcd, uint8_t data)
{
    i2c_cmd_handle_t link = i2c_cmd_link_create();
    i2c_master_start(link);
    i2c_master_write_byte(link, global_lcd_info->i2c_address << 1 | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(link, data, true);
    i2c_master_stop(link);
    esp_err_t error = i2c_master_cmd_begin(global_lcd_info->i2c_port, link, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(link);

    return error;
}

lcd_info_t *lcd_malloc(void)
{
    lcd_info_t *lcd_info = (lcd_info_t *)malloc(sizeof(lcd_info_t));

    if (lcd_info != NULL)
    {
        memset(lcd_info, 0, sizeof(*lcd_info));
        ESP_LOGD(TAG, "malloc lcd_info_t %p", lcd_info);
    }
    else
    {
        ESP_LOGE(TAG, "malloc lcd_info_t failed");
    }

    return lcd_info;
}

void lcd_free(lcd_info_t **lcd_info)
{
    if (lcd_info != NULL && (*lcd_info != NULL))
    {
        ESP_LOGD(TAG, "free lcd_info_t %p", *lcd_info);
        free(*lcd_info);
        *lcd_info = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free lcd_info_t failed");
    }
}

esp_err_t lcd_init(lcd_info_t *lcd_info, uint8_t i2c_port, uint8_t i2c_address, SemaphoreHandle_t *semaphore)
{
    esp_err_t error = ESP_FAIL;
    if (lcd_info != NULL)
    {
        global_lcd_info = lcd_info;

        lcd_info->i2c_port = i2c_port;
        lcd_info->i2c_address = i2c_address;
        lcd_info->semaphore = semaphore;
        lcd_info->init = true;
        lcd_info->hd47780_handle.write_cb = _write_lcd_data;
        lcd_info->hd47780_handle.font = HD44780_FONT_5X8;
        lcd_info->hd47780_handle.lines = 4;
        lcd_info->hd47780_handle.pins.rs = 0;
        lcd_info->hd47780_handle.pins.e = 2;
        lcd_info->hd47780_handle.pins.d4 = 4;
        lcd_info->hd47780_handle.pins.d5 = 5;
        lcd_info->hd47780_handle.pins.d6 = 6;
        lcd_info->hd47780_handle.pins.d7 = 7;
        lcd_info->hd47780_handle.pins.bl = 3;

        error = hd44780_init(&lcd_info->hd47780_handle);
        error = hd44780_switch_backlight(&lcd_info->hd47780_handle, true);

        for (int i = 0; i < 4; i++)
        {
            error = hd44780_upload_character(&lcd_info->hd47780_handle, i, &CHARSET[i]);
        }
    }
    else
    {
        ESP_LOGE(TAG, "lcd_info_t is NULL");
    }

    return error;
}

esp_err_t lcd_write_character(lcd_info_t *lcd_info, char character)
{
    xSemaphoreTake(*lcd_info->semaphore, portMAX_DELAY);
    global_lcd_info = lcd_info;
    hd44780_putc(&lcd_info->hd47780_handle, character);
    xSemaphoreGive(*lcd_info->semaphore);
    return ESP_OK;
}

esp_err_t lcd_write_characters(lcd_info_t *lcd_info, char *characters, int amount)
{
    xSemaphoreTake(*lcd_info->semaphore, portMAX_DELAY);
    global_lcd_info = lcd_info;
    for (int i = 0; i < amount; i++)
    {
        hd44780_putc(&global_lcd_info->hd47780_handle, characters[i]);
    }
    xSemaphoreGive(*lcd_info->semaphore);
    return ESP_OK;
}

esp_err_t lcd_write_string(lcd_info_t *lcd_info, char *string)
{
    xSemaphoreTake(*lcd_info->semaphore, portMAX_DELAY);
    global_lcd_info = lcd_info;
    hd44780_puts(&global_lcd_info->hd47780_handle, string);
    xSemaphoreGive(*lcd_info->semaphore);
    return ESP_OK;
}

esp_err_t lcd_goto(lcd_info_t *lcd_info, uint8_t x, uint8_t y)
{
    xSemaphoreTake(*lcd_info->semaphore, portMAX_DELAY);
    global_lcd_info = lcd_info;
    hd44780_gotoxy(&global_lcd_info->hd47780_handle, x, y);
    xSemaphoreGive(*lcd_info->semaphore);
    return ESP_OK;
}