#ifndef MENU_H
#define MENU_H

#include <esp_event.h>
#include <frame.h>

ESP_EVENT_DECLARE_BASE(MENU_MODULE_EVENT);

/** @enum menu_module_event
 * @brief All the events the menu module can publish.
 */
typedef enum menu_module_event
{
    MENU_DEACTIVATED,
    MENU_UPDATED,
    MENU_ITEM_SELECTED
} menu_module_event_t;

/** @enum menu_item_selected_event
 * @brief The event data for the MENU_ITEM_SELECTED event.
 * @param menu_id The id of the menu publishing this event.
 * @param item_id The id of the item selected.
 */
typedef struct menu_item_selected_event
{
    int menu_id;
    int item_id;
} menu_item_selected_event_t;

/** struct menu_deactivated_event
 * @brief The event data for the MENU_ITEM_DESELECTED event.
 * @param menu_id The id of the menu publishing this event.
 */
typedef struct menu_deactivated_event
{
    int menu_id;
} menu_deactivated_event_t;

/** @enum menu_node_type
 * @brief The type of the menu node.
 */
typedef enum menu_node_type {
    ITEM,
    MENU
} menu_node_type_t;

/** @struct menu_node
 * @brief   This stuct holds information about a menu node and it's neighbors.
 * @param id The unique identifier.
 * @param text The display text.
 * @param type The type of this node.
 * @param prev The previous node in the same hierarchy.
 * @param next The next node in the same hierarchy.
 * @param parent The parent of this node.
 * @param child The first child of this node.
 * @param curr_child_id The identification of the selected child.
 */
typedef struct menu_node
{
    int id;
    char *text;
    menu_node_type_t type;

    struct menu_node *prev;
    struct menu_node *next;
    struct menu_node *parent;
    struct menu_node *child;
    int *curr_child_id;
} menu_node_t;

/** @struct menu_info
 * @brief   The menu root struct, it keeps track of the current node
 *          and it is required for near all menu functions.
 * @param menu_id The unique identifier of the menu.
 * @param root The root node of the menu.
 * @param current The current node of the menu.
 * @param frame The frame to write to.
 */
typedef struct menu_info
{
    int menu_id;
    menu_node_t *root;
    menu_node_t *current;
    frame_t *frame;
} menu_info_t;

/**
 *  @brief  Construct a new menu info instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new menu info instance, or NULL if it cannot be created.
 */
menu_info_t *menu_malloc();

/**
 *  @brief  Delete a existing menu info instance.
 *  @param[in,out] menu_info Pointer to menu info instance that will be freed and set to NULL.
 */
void menu_free(menu_info_t **menu_info);

/**
 *  @brief  Initialize the menu info instance.
 *  @param[in] menu_info Menu info instance that will be initialized.
 *  @param[in] menu_node_t Root menu node of menu info instance.
 *  @param[in] frame_t Frame to draw to.
 *  @param[in] menu_id Id of the menu.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t menu_init(menu_info_t *menu_info, menu_node_t *root, frame_t *frame, int menu_id);

/**
 *  @brief  Construct a new menu node instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new menu node instance, or NULL if it cannot be created.
 */
menu_node_t *menu_node_malloc();

/**
 *  @brief  Delete a existing menu node instance.
 *  @param[in,out] menu_node Pointer to menu node instance that will be freed and set to NULL.
 */
void menu_node_free(menu_node_t **menu_node);

/**
 *  @brief  Initialize a menu node instance.
 *  @param[in] menu_node Menu node instance that will be initialized.
 *  @param[in] id Unique id of the menu node.
 *  @param[in] type Node type.
 *  @param[in] text Text to draw for this node.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t menu_node_init(menu_node_t *menu_node, uint8_t id, menu_node_type_t type, char text[]);

/**
 *  @brief  Add a menu node inside another menu node.
 *  @param[in] menu Menu node that will be the parent.
 *  @param[in] item Menu node that will be the child.
 */
void menu_add_item(menu_node_t *menu, menu_node_t *item);

/**
 *  @brief  Move digital cursor to the previous entry of the current node.
 *  @param[in] menu_info Menu info instance of the menu.
 *  @return Menu node that the cursor moved towards.
 */
int menu_previous(menu_info_t *menu_info);

/**
 *  @brief  Move digital cursor to the next entry of the current node.
 *  @param[in] menu_info Menu info instance of the menu.
 *  @return Menu node that the cursor moved towards.
 */
int menu_next(menu_info_t *menu_info);

/**
 *  @brief  Move digital cursor to the parent entry of the current node.
 *          When the selected child is of type ITEM, it will publish an event indicating it selected the node id.
 *          When the selected child is of type MENU, it will move the cursor to the first entry of that node.
 *  @param[in] menu_info Menu info instance of the menu.
 *  @return Menu node id that is selected
 */
int menu_select(menu_info_t *menu_info);

/**
 *  @brief  Move digital cursor to the child entry of the current node.
 *          When the current node has a parent, it will move the cursor to the parent of the node.
 *          When the current node does not have a parent, it will publish an event indicating it wants to exit the menu.
 *  @param[in] menu_info Menu info instance of the menu.
 *  @return Menu node id that is deselected
 */
int menu_deselect(menu_info_t *menu_info);

#endif