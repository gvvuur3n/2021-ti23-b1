idf_component_register(SRCS "input.c"
                    INCLUDE_DIRS "include"
                    REQUIRES esp_peripherals esp32-mcp23017 esp32-qwiictwist
                    PRIV_REQUIRES)