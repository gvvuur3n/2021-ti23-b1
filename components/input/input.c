#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_peripherals.h>
#include <periph_touch.h>

#include <input.h>

#define GET_BIT(data, bit) ((data >> bit) & 1)

#define TOUCH_VOL_DOWN 4
#define TOUCH_VOL_UP 7
#define TOUCH_SET 9
#define TOUCH_PLAY 8

ESP_EVENT_DEFINE_BASE(INPUT_MODULE_EVENT);

typedef struct
{
    mcp23017_info_t *mcp23017_info;
    qwiictwist_info_t *qwiictwist_info;
} poll_input_task_params_t;

static const char *TAG = "input";

static esp_err_t _touch_event_handler(audio_event_iface_msg_t *event, void *context)
{
    if (event->source_type == PERIPH_ID_TOUCH)
    {
        int button = (int)event->data;

        if (button == TOUCH_VOL_DOWN && event->cmd == 1)
        {
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, VOL_DOWN_PRESSED, NULL, 0, portMAX_DELAY));
        }
        else if (button == TOUCH_VOL_UP && event->cmd == 1)
        {
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, VOL_UP_PRESSED, NULL, 0, portMAX_DELAY));
        }
        else if (button == TOUCH_PLAY && event->cmd == 1)
        {
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, PLAY_PRESSED, NULL, 0, portMAX_DELAY));
        }
        else if (button == TOUCH_SET && event->cmd == 1)
        {
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, SET_PRESSED, NULL, 0, portMAX_DELAY));
        }
    }

    return ESP_OK;
}

static void _poll_input_task(void *parameter)
{
    poll_input_task_params_t *poll_input_task_params = (poll_input_task_params_t *)parameter;
    mcp23017_info_t *mcp23017_info = poll_input_task_params->mcp23017_info;
    qwiictwist_info_t *qwiictwist_info = poll_input_task_params->qwiictwist_info;

    uint8_t previous = 0;
    uint8_t buffer;
    input_module_event_t mapping[] = {
        MENU_UP_PRESSED,
        MENU_DOWN_PRESSED,
        MENU_SELECT_PRESSED,
        MENU_DESELECT_PRESSED};

    while (1)
    {
        ESP_ERROR_CHECK(mcp23017_port_read(mcp23017_info, PORTA, &buffer));

        for (uint8_t i = 0; i < 4; i++)
        {
            if ((GET_BIT(previous, i) == 0) && (GET_BIT(buffer, i) == 1))
            {
                // check if previous bit is low and buffer bit is high

                ESP_LOGV(TAG, "Button %d pressed!", i);
                ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, mapping[i], NULL, 0, portMAX_DELAY));
            }
        }

        previous = buffer;

        bool twist_clicked = false;
        bool twist_moved = false;

#if CONFIG_ESP_HAS_QWIIC_TWIST
        ESP_ERROR_CHECK(qwiictwist_is_clicked(qwiictwist_info, &twist_clicked));
        ESP_ERROR_CHECK(qwiictwist_is_moved(qwiictwist_info, &twist_moved));
#endif

        if (twist_clicked)
        {
            ESP_LOGV(TAG, "Twist clicked!");
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, MENU_SELECT_PRESSED, NULL, 0, portMAX_DELAY));
        }

        if (twist_moved)
        {
            ESP_LOGV(TAG, "Twist moved!");

            int16_t twist_difference = 0;
            ESP_ERROR_CHECK(qwiictwist_get_difference(qwiictwist_info, &twist_difference));

            bool is_down = twist_difference < -100;

            ESP_LOGV(TAG, "Moved %d", twist_difference);
            ESP_ERROR_CHECK(esp_event_post(INPUT_MODULE_EVENT, is_down ? MENU_DOWN_PRESSED : MENU_UP_PRESSED, NULL, 0, portMAX_DELAY));
        }

        vTaskDelay(200 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

static void _init_touch_buttons(esp_periph_set_handle_t set_handle)
{
    ESP_LOGI(TAG, "Initialzing touch input");

    ESP_ERROR_CHECK(esp_periph_set_register_callback(set_handle, _touch_event_handler, NULL));
    periph_touch_cfg_t touch_cfg = {
        .long_tap_time_ms = 2000,
        .tap_threshold_percent = 70,
        .touch_mask = TOUCH_PAD_SEL4 | TOUCH_PAD_SEL7 | TOUCH_PAD_SEL8 | TOUCH_PAD_SEL9,
    };
    esp_periph_handle_t touch = periph_touch_init(&touch_cfg);
    ESP_ERROR_CHECK(esp_periph_start(set_handle, touch));
}

static void _init_gpio_buttons(mcp23017_info_t *mcp23017_info)
{
    ESP_LOGI(TAG, "Initialzing gpio input");

    ESP_ERROR_CHECK(mcp23017_port_set_interrupt(mcp23017_info, PORTA, 0x00));
    ESP_ERROR_CHECK(mcp23017_port_set_direction(mcp23017_info, PORTA, 0xFF));
    ESP_ERROR_CHECK(mcp23017_port_set_latch(mcp23017_info, PORTA, 0x0F));
    ESP_ERROR_CHECK(mcp23017_port_set_polarity(mcp23017_info, PORTA, 0xFF));
}

void init_inputs(esp_periph_set_handle_t set_handle, mcp23017_info_t *mcp23017_info, qwiictwist_info_t *qwiictwist_info)
{
    _init_touch_buttons(set_handle);
    _init_gpio_buttons(mcp23017_info);

    poll_input_task_params_t *params = (poll_input_task_params_t *)malloc(sizeof(poll_input_task_params_t));
    params->mcp23017_info = mcp23017_info;
    params->qwiictwist_info = qwiictwist_info;

    // ESP_ERROR_CHECK(qwiictwist_set_color(qwiictwist_info, 255, 255, 255));
    // ESP_ERROR_CHECK(qwiictwist_set_connect_color(qwiictwist_info, 0, 0, 0));

    xTaskCreate(_poll_input_task, "pollInputTask", 4096, params, 1, NULL);
}