#define LOG_LOCAL_LEVEL ESP_LOG_WARN

#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"

#include <frame.h>
#include <lcd.h>

static const char *TAG = "Frame module";

static esp_err_t _frame_group_draw(frame_group_t *frame_group)
{
    esp_err_t error = ESP_OK;

    ESP_LOGD(TAG, "Draw frame group");

    // Fill the buffer with spaces
    char buffer[MAX_FRAME_WIDTH];
    memset(buffer, (int)' ', MAX_FRAME_WIDTH);

    // Draw the buffer containing only spaces
    for (int i = 0; i < MAX_FRAME_HEIGHT; i++)
    {
        error = frame_group->root->draw_cb(0, i, buffer, MAX_FRAME_WIDTH);
    }

    // Draw all frames contained in this frame group
    for (int i = 0; i < frame_group->amount; i++)
    {
        frame_flush(frame_group->frames[i]);
    }

    return error;
}

frame_root_t *frame_root_malloc()
{
    frame_root_t *frame_root = (frame_root_t *)malloc(sizeof(frame_root_t));
    if (frame_root != NULL)
    {
        memset(frame_root, 0, sizeof(*frame_root));

        ESP_LOGD(TAG, "malloc frame_root_t %p", frame_root);
    }
    else
    {
        ESP_LOGE(TAG, "malloc frame_root_t failed");
    }

    return frame_root;
}

void frame_root_free(frame_root_t **frame_root)
{
    if (frame_root != NULL && (*frame_root != NULL))
    {
        ESP_LOGD(TAG, "free frame_root_t %p", *frame_root);

        free(*frame_root);
        *frame_root = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free frame_root_t failed");
    }
}

esp_err_t frame_root_init(frame_root_t *frame_root, frame_draw_cb_t draw_cb)
{
    esp_err_t error;

    if (frame_root != NULL)
    {
        frame_root->current_id = -1;
        frame_root->draw_cb = draw_cb;

        error = ESP_OK;
    }
    else
    {
        ESP_LOGE(TAG, "frame_root_t is NULL");
    }

    return error;
}

frame_group_t *frame_group_malloc()
{
    frame_group_t *frame_group = (frame_group_t *)malloc(sizeof(frame_group_t));
    if (frame_group != NULL)
    {
        memset(frame_group, 0, sizeof(*frame_group));

        ESP_LOGD(TAG, "malloc frame_group_t %p", frame_group);
    }
    else
    {
        ESP_LOGE(TAG, "malloc frame_group_t failed");
    }

    return frame_group;
}

void frame_group_free(frame_group_t **frame_group)
{
    if (frame_group != NULL && (*frame_group != NULL))
    {
        ESP_LOGD(TAG, "free frame_group_t %p", *frame_group);

        free((*frame_group)->frames);
        free(*frame_group);
        *frame_group = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free frame_group_t failed");
    }
}

esp_err_t frame_group_init(frame_group_t *frame_group, frame_root_t *root, int id)
{
    esp_err_t error;

    if (frame_group != NULL)
    {
        frame_group->id = id;
        frame_group->amount = 0;

        // Malloc at least enough memory for 3 frame pointers
        frame_group->amount_max = 3;
        frame_group->frames = malloc(sizeof(frame_t *) * 3);

        frame_group->root = root;

        error = ESP_OK;
    }
    else
    {
        ESP_LOGE(TAG, "frame_group_t is NULL");
    }

    return error;
}

esp_err_t frame_group_add(frame_group_t *frame_group, frame_t *frame)
{
    // check if memory needs to be resized
    if (frame_group->amount + 1 == frame_group->amount_max)
    {
        int size = sizeof(frame_t *) * (frame_group->amount + 3);

        // resize memory with 3 additional entries
        frame_group->frames = (frame_t **)realloc(frame_group->frames, size);
        frame_group->amount_max = frame_group->amount_max + 3;
    }

    frame->parent = frame_group;

    // add the frame pointer
    frame_group->frames[frame_group->amount] = frame;
    frame_group->amount = frame_group->amount + 1;

    return ESP_OK;
}

esp_err_t frame_group_clear(frame_group_t *frame_group)
{
    assert(frame_group != NULL);

    // check if it needs to clear
    if (frame_group->amount == 0)
        return ESP_OK;

    // free the memory of every frame contained in the group
    for (int i = 0; i < frame_group->amount; i++)
    {
        frame_free(&frame_group->frames[i]);
    }

    // reallocate memory back to 3 entries.
    frame_group->frames = (frame_t **)realloc(frame_group->frames, sizeof(frame_t) * 3);
    frame_group->amount_max = 3;
    frame_group->amount = 0;

    return ESP_OK;
}

esp_err_t frame_group_activate(frame_group_t *frame_group)
{
    esp_err_t error = ESP_OK;
    assert(frame_group != NULL);
    frame_root_t *frame_root = frame_group->root;

    // check if the current group is not the same as the one given
    bool refresh = frame_root->current_id != frame_group->id;
    frame_root->current_id = frame_group->id;

    if (refresh)
    {
        // draw the frame group if it is needed
        error = _frame_group_draw(frame_group);
    }

    return error;
}

frame_t *frame_malloc()
{
    frame_t *frame = (frame_t *)malloc(sizeof(frame_t));
    if (frame != NULL)
    {
        memset(frame, 0, sizeof(*frame));

        ESP_LOGD(TAG, "malloc frame_t %p", frame);
    }
    else
    {
        ESP_LOGE(TAG, "malloc frame_t failed");
    }

    return frame;
}

void frame_free(frame_t **frame)
{
    if (frame != NULL && (*frame != NULL))
    {
        ESP_LOGD(TAG, "free frame_t %p", *frame);

        free((*frame)->value);
        free(*frame);
        *frame = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free frame_t failed");
    }
}

esp_err_t frame_init(frame_t *frame, int x, int y, int width, int height)
{
    esp_err_t error;

    if (frame != NULL)
    {
        frame->x = x;
        frame->y = y;
        frame->width = width;
        frame->height = height;
        frame->parent = NULL;

        // allocate enough memory for the buffer
        size_t value_size = (sizeof(char) * height * width);
        frame->value = (char *)malloc(value_size);
        memset(frame->value, (int)' ', value_size);

        error = ESP_OK;
    }
    else
    {
        ESP_LOGE(TAG, "frame_t is NULL");
    }

    return error;
}

esp_err_t frame_write_string(frame_t *frame, int x, int y, char *string)
{
    esp_err_t error = ESP_OK;

    // fix boundary
    if (x >= frame->width)
        x = frame->width - 1;
    if (y >= frame->height)
        y = frame->height - 1;

    // calculate position within the buffer
    uint8_t position = (y * frame->width) + x;
    // calculate how much space is left after the position
    size_t copy_length = (frame->width * frame->height) - position;
    size_t string_length = strlen(string);

    // determine correct copy length
    if (string_length < copy_length)
        copy_length = string_length;

    // copy the characters from the string to the buffer
    memcpy(&frame->value[position], string, copy_length);

    return error;
}

esp_err_t frame_write_character(frame_t *frame, int x, int y, char character)
{
    esp_err_t error = ESP_OK;

    // fix boundary
    if (x >= frame->width)
        x = frame->width - 1;
    if (y >= frame->height)
        y = frame->height - 1;

    // calculate position within the buffer
    uint8_t position = (y * frame->width) + x;
    // write the character to the buffer
    frame->value[position] = character;

    return error;
}

esp_err_t frame_clear(frame_t *frame)
{
    // fill the entire buffer with spaces
    char string[frame->width * frame->height + 1];
    memset(string, (int)' ', frame->width * frame->height);
    return frame_write_string(frame, 0, 0, string);
}

esp_err_t frame_flush(frame_t *frame)
{
    esp_err_t error = ESP_OK;

    // check if the frame is within an active frame group
    if (frame->parent->root->current_id == frame->parent->id)
    {
        // traverse all vertical lines
        for (int i = 0; i < frame->height; i++)
        {
            // calculate x and y positions for this line
            int x = frame->x;
            int y = frame->y + i;

            // create and fill a character array for this line
            char string[frame->width + 1];
            memcpy(string, &frame->value[i * frame->width], frame->width);

            // use the drawing callback to write this line
            error = frame->parent->root->draw_cb(x, y, string, frame->width);
        }
    }

    return error;
}