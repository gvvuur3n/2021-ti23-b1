#include <audio_module.h>
#include <radio.h>
#include <string.h>

static void _event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    radio_info_t *info = (radio_info_t *)arg;

    if (event_base == AUDIO_MODULE_EVENT && event_id == NO_AUDIO)
    {
        if (info->is_set && info->is_playing)
        {
            play_http(info->radio_channel.url);
        }
    }
}

radio_info_t *init_radio(frame_t *frame)
{
    radio_info_t *radio = (radio_info_t *)malloc(sizeof(radio_info_t));
    memset(radio, 0, sizeof(radio_info_t));

    radio->idle_frame = frame;
    radio->is_playing = false;
    radio->is_set = false;

    frame_write_string(radio->idle_frame, 0, 0, "no channel");
    frame_flush(radio->idle_frame);

    esp_event_handler_register(AUDIO_MODULE_EVENT, NO_AUDIO, _event_handler, radio);

    return radio;
}

void set_radio(radio_info_t *info, radio_t channel)
{
    frame_t *idle = info->idle_frame;
    frame_clear(idle);
    frame_write_string(idle, 0, 0,channel.channel_name);
    frame_flush(idle);

    info->radio_channel = channel;
    info->is_set = true;
    pause_radio(info);
    play_radio(info);
}

void play_radio(radio_info_t *info)
{
    if (info->is_set && !info->is_playing)
    {
        play_http(info->radio_channel.url);
        info->is_playing = true;
    }
}

void pause_radio(radio_info_t *info)
{
    if (info->is_playing)
    {
        stop_audio();
        info->is_playing = false;
    }
}