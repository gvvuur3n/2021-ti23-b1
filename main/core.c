#include <string.h>
#include <stdio.h>
#include <time.h>
#include <dirent.h>

#include <nvs_flash.h>
#include <sdkconfig.h>
#include <esp_peripherals.h>
#include <periph_sdcard.h>
#include <periph_touch.h>

#include <board.h>
#include <board_pins_config.h>

#include <esp_log.h>

#include <mcp23017.h>

#include <i2cdev.h>
#include <lcd.h>
#include <input.h>
#include <frame.h>
#include <menu.h>

#include "wifi_module.h"
#include "ip_module.h"
#include "time_module.h"
#include "weather_module.h"
#include "audio_module.h"
#include "core.h"
#include "setup.h"
#include <set_alarm.h>
#include <set_radio.h>
#include <set_memo.h>
#include <idle.h>
#include <tts.h>
#include <main_menu.h>

static const char TAG[] = "core";

static bool time_started = false;

static screen_t *active_screen;

screen_t *idle_screen;
screen_t *main_menu_screen;
screen_t *set_radio_screen;
screen_t *set_alarm_screen;
screen_t *set_memo_screen;

alarm_info_t *alarm_info;
radio_info_t *radio_info;
set_memo_info_t *memo_info;
static idle_info_t *idle_info;

static int current_hour = -1;
static bool hour_passed = false;
static int temperature = 0;

static void _time_task()
{
    while (1)
    {
        time_t rawtime;
        time(&rawtime);

        struct tm *time_info;
        time_info = localtime(&rawtime);

        if (time_info->tm_hour != current_hour)
        {
            hour_passed = false;
        }

        if (alarm_info->time != NULL && time_info->tm_min != alarm_info->time->tm_min)
        {
            if (time_info->tm_min == 0 && !hour_passed && time_info->tm_hour != alarm_info->time->tm_hour)
            {
                play_time(*time_info);
                current_hour = time_info->tm_hour;
                hour_passed = true;
            }
        }

        update_time(idle_info, time_info);
        check_alarm(alarm_info, time_info);

        vTaskDelay(1000 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);
}

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == INPUT_MODULE_EVENT)
    {
        stop_alarm(alarm_info);

        if (active_screen != NULL && active_screen->input_handler != NULL)
        {
            active_screen->input_handler(active_screen, (input_module_event_t)event_id);
        }

        if (event_id == PLAY_PRESSED)
        {
            time_t rawtime;
            time(&rawtime);

            struct tm *time_info;
            time_info = localtime(&rawtime);

            play_time(*time_info);
        }
        else if (event_id == SET_PRESSED)
        {
            play_temperature(temperature);
        }
    }
    else if (event_base == TIME_MODULE_EVENT && event_id == TIME_SYNCED)
    {
        if (!time_started)
        {
            xTaskCreate(_time_task, "timeTask", 4096, NULL, 4, NULL);
            time_started = true;
        }
    }
    else if (event_base == WEATHER_MODULE_EVENT && event_id == WEATHER_DATA_FETCHED)
    {
        weather_data_t *data = (weather_data_t *)event_data;

        temperature = (int)data->temperature;
    }
}

screen_t *screen_malloc()
{
    screen_t *screen = (screen_t *)malloc(sizeof(screen_t));
    memset(screen, 0, sizeof(screen_t));

    return screen;
}

void screen_free(screen_t **screen)
{
    if (screen != NULL && *screen != NULL)
    {
        free(*screen);
        free(screen);

        *screen = NULL;
    }
}

void screen_activate(screen_t *screen)
{
    if (active_screen != NULL)
    {
        if (active_screen->exit_handler != NULL)
        {
            ESP_ERROR_CHECK(active_screen->exit_handler(active_screen));
        }
    }

    if (screen->frame_group != NULL)
    {
        frame_group_activate(screen->frame_group);
    }

    ESP_LOGV(TAG, "Screen activated");
    active_screen = screen;

    if (active_screen->enter_handler != NULL)
    {
        ESP_ERROR_CHECK(active_screen->enter_handler(active_screen));
    }
}

void setup_core()
{
    ESP_ERROR_CHECK(esp_event_handler_register(TIME_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(INPUT_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(WEATHER_MODULE_EVENT, WEATHER_DATA_FETCHED, event_handler, NULL));

    frame_root_t *frame_root = frame_root_malloc();
    ESP_ERROR_CHECK(frame_root_init(frame_root, draw_frame));

    active_screen = NULL;

    idle_screen = init_idle_screen(frame_root, 9990);
    idle_info = (idle_info_t *)idle_screen->info;

    alarm_info = init_alarm("alarm", idle_info->alarm_frame);
    radio_info = init_radio(idle_info->radio_frame);

    main_menu_screen = init_main_menu_screen(frame_root, 9991);
    set_radio_screen = init_set_radio_screen(frame_root, 9999, radio_info);
    set_alarm_screen = init_set_alarm_screen(frame_root, 9999, alarm_info);
    set_memo_screen = init_set_memo_screen(frame_root, 9237);

    screen_activate(idle_screen);

    set_volume(30);
}
