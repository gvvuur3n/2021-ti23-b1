#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <freertos/FreeRTOS.h>
#include <assert.h>
#include <string.h>
#include <esp_log.h>

#include <input.h>
#include <set_radio.h>
#include <menu.h>
#include <audio_module.h>

#define SET_RADIO_MENU_ID 0x00
#define SET_RADIO_MENU_ID_ROOT 0x00

static const char TAG[] = "radio";

static void _select_channel(set_radio_info_t *info, int id)
{
    ESP_LOGV(TAG, "Select current channel");
    radio_t channel = info->channels[id];

    set_radio(info->radio_info, channel);
}

static esp_err_t _screen_enter(screen_t *screen)
{
    return ESP_OK;
}

static esp_err_t _screen_exit(screen_t *screen)
{
    return ESP_OK;
}

static esp_err_t _screen_input(screen_t *screen, input_module_event_t event)
{
    set_radio_info_t *info = (set_radio_info_t *)screen->info;

    if (event == MENU_UP_PRESSED)
    {
        // move current channel down
        menu_previous(info->menu);
    }
    else if (event == MENU_DOWN_PRESSED)
    {
        // move current channel up
        menu_next(info->menu);
    }
    else if (event == MENU_SELECT_PRESSED)
    {
        // select current channel
        _select_channel(info, menu_select(info->menu));
    }
    else if (event == MENU_DESELECT_PRESSED)
    {
        // exit screen
        screen_activate(main_menu_screen);
    }

    return ESP_OK;
}

screen_t *init_set_radio_screen(frame_root_t *frame_root, int id, radio_info_t *radio_info)
{
    assert(frame_root != NULL);
    assert(id >= 0);
    assert(radio_info != NULL);

    screen_t *screen = screen_malloc();
    frame_group_t *frame_group = frame_group_malloc();
    frame_group_init(frame_group, frame_root, id);
    screen->frame_group = frame_group;

    set_radio_info_t *info = (set_radio_info_t *)malloc(sizeof(set_radio_info_t));
    memset(info, 0, sizeof(set_radio_info_t));

    strcpy(info->channels[0].channel_name, "Radio Sputnik");
    strcpy(info->channels[0].url, "https://radiosputnik.nl:8443/");
    strcpy(info->channels[1].channel_name, "Radio 3FM");
    strcpy(info->channels[1].url, "https://icecast.omroep.nl/3fm-bb-mp3");
    strcpy(info->channels[2].channel_name, "100\% NL");
    strcpy(info->channels[2].url, "https://stream.100p.nl/100pctnl.mp3");
    strcpy(info->channels[3].channel_name, "Slam!");
    strcpy(info->channels[3].url, "https://stream.slam.nl/web14_mp3");
    strcpy(info->channels[4].channel_name, "Radio 10");
    strcpy(info->channels[4].url, "http://19993.live.streamtheworld.com/RADIO10.mp3");
    info->size = 5;
    info->radio_info = radio_info;

    info->menu_frame = frame_malloc();
    frame_init(info->menu_frame, 0, 0, 20, 4);
    frame_group_add(screen->frame_group, info->menu_frame);

    info->menu = menu_malloc();
    menu_node_t *root = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(root, SET_RADIO_MENU_ID_ROOT, MENU, "ROOT"));

    for (int i = 0; i < info->size; i++)
    {
        menu_node_t *channel = menu_node_malloc();
        ESP_ERROR_CHECK(menu_node_init(channel, i, ITEM, info->channels[i].channel_name));
        menu_add_item(root, channel);
    }

    ESP_ERROR_CHECK(menu_init(info->menu, root, info->menu_frame, SET_RADIO_MENU_ID));

    screen->info = info;
    screen->enter_handler = _screen_enter;
    screen->exit_handler = _screen_exit;
    screen->input_handler = _screen_input;

    return screen;
}