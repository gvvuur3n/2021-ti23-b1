#include <time.h>
#include <freertos/FreeRTOS.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <wifi_module.h>
#include <weather_module.h>
#include <audio_module.h>
#include <menu.h>
#include "main_menu.h"

#include "idle.h"

static void _event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
static void _print_bar(frame_t *frame, int value, int max);
static void _print_time(idle_info_t *idle_info, struct tm *tm);

static void _event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    idle_info_t *info = (idle_info_t *)arg;

    if (event_base == WIFI_MODULE_EVENT && event_id == WIFI_CONNECTED)
    {
        frame_write_string(info->conn_frame, 0, 0, "CONN");
        frame_flush(info->conn_frame);
    }
    else if (event_base == WIFI_MODULE_EVENT && event_id == WIFI_DISCONNECTED)
    {
        frame_write_string(info->conn_frame, 0, 0, "NCON");
        frame_flush(info->conn_frame);
    }
    else if (event_base == WEATHER_MODULE_EVENT && event_id == WEATHER_DATA_FETCHED)
    {
        weather_data_t *weather_data = (weather_data_t *)event_data;
        char string[20];
        sprintf(string, "%2.1fC", weather_data->temperature);

        frame_write_string(info->temperature_frame, 0, 0, string);
        frame_flush(info->temperature_frame);
    }
    else if (event_base == AUDIO_MODULE_EVENT && event_id == VOLUME_CHANGED)
    {
        _print_bar(info->volume_frame, get_volume(), 100);
    }
}

static void _print_bar(frame_t *frame, int value, int max)
{
    int length = frame->width;
    char string[frame->width + 1];
    int blocks = value / (max / (length * 1.0));

    int i = 0;
    string[i] = '[';
    for (i = 1; i < length - 1; i++)
    {
        string[i] = i < blocks ? '*' : '-';
    }
    string[i] = ']';

    frame_write_string(frame, 0, 0, string);
    frame_flush(frame);
}

static void _print_time(idle_info_t *idle_info, struct tm *tm)
{
    char date_string[100];
    char time_string[100];

    strftime(date_string, sizeof(date_string) - 1, "%d %m %Y", tm);
    strftime(time_string, sizeof(time_string) - 1, "%H:%M:%S", tm);

    frame_write_string(idle_info->time_frame, 0, 0, date_string);
    frame_write_string(idle_info->time_frame, 0, 1, time_string);
    frame_flush(idle_info->time_frame);
}

void update_time(idle_info_t *idle_info, struct tm *tm)
{
    _print_time(idle_info, tm);
}

static esp_err_t _screen_enter(screen_t *screen)
{
    idle_info_t *info = (idle_info_t *)screen->info;

    return ESP_OK;
}

static esp_err_t _screen_exit(screen_t *screen)
{
    idle_info_t *info = (idle_info_t *)screen->info;

    return ESP_OK;
}

static esp_err_t _screen_input(screen_t *screen, input_module_event_t event)
{
    idle_info_t *info = (idle_info_t *)screen->info;

    if (event == MENU_UP_PRESSED)
    {
    }
    else if (event == MENU_DOWN_PRESSED)
    {
    }
    else if (event == MENU_SELECT_PRESSED)
    {
        // open main menu
        screen_activate(main_menu_screen);
    }
    else if (event == MENU_DESELECT_PRESSED)
    {
    }

    return ESP_OK;
}

screen_t *init_idle_screen(frame_root_t *frame_root, int id)
{
    assert(frame_root != NULL);
    assert(id >= 0);

    screen_t *screen = screen_malloc();
    frame_group_t *frame_group = frame_group_malloc();
    frame_group_init(frame_group, frame_root, id);
    screen->frame_group = frame_group;

    idle_info_t *info = (idle_info_t *)malloc(sizeof(idle_info_t));
    memset(info, 0, sizeof(idle_info_t));

    info->temperature_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->temperature_frame, MAX_FRAME_WIDTH - 4, 2, 4, 1));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->temperature_frame));

    info->volume_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->volume_frame, 0, 0, MAX_FRAME_WIDTH, 1));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->volume_frame));

    info->time_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->time_frame, 5, 1, 10, 2));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->time_frame));

    info->conn_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->conn_frame, MAX_FRAME_WIDTH - 4, 1, 4, 1));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->conn_frame));

    info->radio_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->radio_frame, 0, 3, 14, 1));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->radio_frame));

    info->alarm_frame = frame_malloc();
    ESP_ERROR_CHECK(frame_init(info->alarm_frame, MAX_FRAME_WIDTH - 5, 3, 5, 1));
    ESP_ERROR_CHECK(frame_group_add(screen->frame_group, info->alarm_frame));

    frame_write_string(info->time_frame, 0, 0, "Awaiting");
    frame_write_string(info->time_frame, 0, 1, "WiFi");
    frame_flush(info->time_frame);

    esp_event_handler_register(WIFI_MODULE_EVENT, ESP_EVENT_ANY_ID, _event_handler, info);
    esp_event_handler_register(WEATHER_MODULE_EVENT, ESP_EVENT_ANY_ID, _event_handler, info);
    esp_event_handler_register(AUDIO_MODULE_EVENT, ESP_EVENT_ANY_ID, _event_handler, info);

    screen->info = info;
    screen->enter_handler = _screen_enter;
    screen->exit_handler = _screen_exit;
    screen->input_handler = _screen_input;

    return screen;
}