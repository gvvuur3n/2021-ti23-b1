#ifndef MAIN_ALARM_H
#define MAIN_ALARM_H

#include <time.h>
#include <core.h>
#include <frame.h>

/**
 * @brief sets the functions of the alarm.
 * 
 */
typedef esp_err_t(*set_alarm_func_t)(struct tm time_info);

/**
 * @struct alarm_info_t
 * @brief this contains the info of the alarm. The sound, time and the frame of the screen.
 * 
 * @param time contains the current date and time of the user.
 * @param sound is the sound which it makes when the alarm goes off.
 * @param frame is the frame for the lcd screen of the alarm menu.
 * @param is_playing is true if its playing and false if its not playing.
 * 
 */
typedef struct {
    struct tm *time;
    char *sound;
    frame_t *frame;
    bool is_playing;
} alarm_info_t;

/**
 * @brief this method initialises the alarm screen and the alarm sound.
 * 
 * @param alarm_sound the sound which the alarm will make when it goes off.
 * @param alarm_frame the frame of the alarm menu.
 * @return This returns the alarm_info in which the sound and frame had been changed.
 */
alarm_info_t *init_alarm(char *alarm_sound, frame_t *alarm_frame);

/**
 * @brief This method plays the sound when the alarm goes off.
 * 
 * @param info info is a variable of the struct alarm_info_t in which the 
 * chosen sound is located.
 */
void play_alarm(alarm_info_t *info);

/**
 * @brief This method stops the alarm and the audio and removes the alarm.
 * 
 * @param info info is the alarm which has been stopt.
 */
void stop_alarm(alarm_info_t *info);

/**
 * @brief This method checks the alarm to see if the alarm time has been reached.
 * 
 * @param info location of the time of the alarm.
 * @param time contains the date and time.
 */
void check_alarm(alarm_info_t *info, struct tm *time);

/**
 * @brief This method sets the alarm time to the chosen time of the user.
 * 
 * @param info the variable which contains the data of that timer.
 * @param time Used to compare with current time to check the requested alarm.
 */
void set_alarm(alarm_info_t *info, struct tm *time);

/**
 * @brief This method removes the alarm.
 * 
 * @param info this variable, containing the info of the timer, will be reset.
 */
void remove_alarm(alarm_info_t *info);

#endif