#ifndef MAIN_SET_MEMO_H
#define MAIN_SET_MEMO_H

#include <core.h>
#include <radio.h>
#include <menu.h>

typedef enum
{
    PLAY,
    REMOVE
} set_memo_mode_t;

typedef struct
{
    menu_info_t *menu;
    frame_t *menu_frame;
    char **memos;
    bool has_memos;
    set_memo_mode_t mode;
} set_memo_info_t;

screen_t *init_set_memo_screen(frame_root_t *frame_root, int id);
void set_memo_screen_mode(set_memo_info_t *info, set_memo_mode_t mode);

#endif