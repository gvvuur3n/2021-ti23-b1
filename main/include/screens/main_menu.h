#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <main.h>
#include <core.h>
#include <frame.h>
#include <menu.h>

/**
 * @struct main_menu_info_t
 * @brief contains the info of the main menu for the lcd screen.
 * 
 * @param menu_frame Frame which is used to show the main menu.
 * @param menu the info of the main menu which includes the button settings.
 * 
 */
typedef struct
{
    frame_t *menu_frame;
    menu_info_t *menu;
} main_menu_info_t;

/**
 * @brief initialises the main menu screen
 * 
 * @param frame_root Root object of the frame structure
 * @param id Unique id of the frame_group
 * @return Instance of screen object.
 */
screen_t *init_main_menu_screen(frame_root_t *frame_root, int id);

#endif