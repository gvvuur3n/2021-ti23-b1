#ifndef MAIN_IDLE_H
#define MAIN_IDLE_H

#include <core.h>
#include <frame.h>

/**
 * @struct idle_info_t
 * @brief Contains the info of the screen while its idle.
 * 
 * @param idle_screen this is the screen which will be shown on the lcd.
 * @param temperature_frame Frame which is used to show the temperature.
 * @param volume_frame Frame which is used to show the volume.
 * @param time_frame Frame which is used to show the time.
 * @param conn_frame Frame which is used to show the wifi connection status.
 * @param radio_frame Frame which is used to show the radio screen.
 * @param alarm_frame Frame which is used to show the alarm screen.
 * 
 */
typedef struct
{
    screen_t *idle_screen;
    frame_t *temperature_frame;
    frame_t *volume_frame;
    frame_t *time_frame;
    frame_t *conn_frame;
    frame_t *radio_frame;
    frame_t *alarm_frame;
} idle_info_t;

/**
 * @brief initialises all the idle screens.
 * 
 * @param frame_root Root object of the frame structure
 * @param id Unique id of the frame_group
 * @return Instance of screen object.
 */
screen_t *init_idle_screen(frame_root_t *frame_root, int id);

/**
 * @brief This method changes the used time on the screen to the current time.
 * 
 * @param idle_info idle info contains the info of the screens while its idle
 * @param tm This is used to get the current time.
 */
void update_time(idle_info_t *idle_info, struct tm *tm);

#endif