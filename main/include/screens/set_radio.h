#ifndef MAIN_SET_RADIO_H
#define MAIN_SET_RADIO_H

#include <core.h>
#include <menu.h>
#include <radio.h>

/**
 * @struct set_radio_info_t
 * @brief contains the data of the radio screen. 
 * It also contains the list of radio channels.
 * 
 * @param channels contains the hardcoded radio channels which can be selected.
 * @param size is the size of the radio array. aka the amount of radio channels
 * @param menu the info of the main menu which includes the button settings.
 * @param menu_frame Frame which is used to show the main menu.
 * @param radio_info contains the info of each radio channel.
 * 
 */
typedef struct{
    radio_t channels[5];
    int size;
    menu_info_t *menu;
    frame_t *menu_frame;
    radio_info_t *radio_info;
} set_radio_info_t;

/**
 * @brief initialises the set_radio screen.
 * 
 * @param frame_root Root object of the frame structure
 * @param id Unique id of the frame_group
 * @param radio_info contains the data of the set radio screen.
 * @return Instance of screen object.
 */
screen_t *init_set_radio_screen(frame_root_t *frame_root, int id, radio_info_t *radio_info);

#endif