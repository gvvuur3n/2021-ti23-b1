#ifndef MAIN_SETUP_H
#define MAIN_SETUP_H

#define I2C_MASTER_NUM I2C_NUM_0
#define I2C_MCP23017_ADDRESS 0x20
#define I2C_LCD_ADDRESS 0x27

#include <esp_err.h>

/**
 * @brief 
 * 
 * @param x 
 * @param y 
 * @param string 
 * @param length 
 * @return esp_err_t 
 */
esp_err_t draw_frame(int x, int y, char *string, int length);

/**
 * @brief Initializes the drivers and modules of the project.
 * 
 */
void setup_components();

#endif