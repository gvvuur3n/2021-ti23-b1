#ifndef TTS_H
#define TTS_H

/**
 * @brief Plays the time via the microjack output on the device.
 * 
 * @param time Current time.
 */
void play_time(struct tm time);

/**
 * @brief Plays the temperature via the microjack output on the device.
 * 
 * @param temperature Current temperature.
 */
void play_temperature(int temperature);

#endif