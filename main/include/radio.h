#ifndef MAIN_RADIO_H
#define MAIN_RADIO_H

#include <frame.h>

/**
 * @brief Stores the name and url of a radiochannel.
 * 
 */
typedef struct
{
    char channel_name[20];
    char url[50];
} radio_t;

/**
 * @struct radio_info_t
 * @brief Stores all the data the radio module needs.
 * 
 * @param radio_channel Current radio channel.
 * @param idle_frame Frame reserved for the radio on the idle_screen.
 * @param is_playing State of playing.
 * @param is_set Is true when radio_channel has been set, otherwise false.
 */
typedef struct
{
    radio_t radio_channel;
    frame_t *idle_frame;
    bool is_playing;
    bool is_set;
} radio_info_t;

/**
 * @brief Initializes the radio_module.
 * 
 * @param frame Frame on the idle_screen, reserved for the radio.
 * @return Instance of radio info object.
 */
radio_info_t *init_radio(frame_t *frame);

/**
 * @brief Selects the current radiochannel, and plays that channel.
 * 
 * @param info Data object of the radio_module.
 * @param channel Current channel.
 */
void set_radio(radio_info_t *info, radio_t channel);

/**
 * @brief Plays the current selected channel if a channel has been selected.
 * 
 * @param info Data object of the radio_module.
 */
void play_radio(radio_info_t *info);

/**
 * @brief Stops the current pipeline, and sets is_playing to false.
 * 
 * @param info Data object of the radio_module.
 */
void pause_radio(radio_info_t *info);
#endif