#include <string.h>
#include <stdio.h>
#include <time.h>
#include <nvs_flash.h>
#include <sdkconfig.h>
#include <esp_peripherals.h>
#include <periph_sdcard.h>
#include <periph_touch.h>
#include <board.h>
#include <board_pins_config.h>
#include <esp_log.h>
#include <mcp23017.h>
#include <lcd.h>
#include <input.h>
#include <frame.h>
#include <menu.h>
#include "wifi_module.h"
#include "ip_module.h"
#include "time_module.h"
#include "weather_module.h"
#include "audio_module.h"
#include "sound-detect_module.h"
#include <qwiictwist.h>

#include "setup.h"

static const char TAG[] = "setup";
static SemaphoreHandle_t *i2c_semaphore;
static mcp23017_info_t *mcp23017_info;
static qwiictwist_info_t *qwiictwist_info;
static lcd_info_t *lcd_info;
static esp_periph_set_handle_t set;
static audio_board_handle_t board;
static SemaphoreHandle_t frame_semaphore;

static void _init_drivers()
{
    ESP_LOGI(TAG, "Initializing ADF board");
    board = audio_board_init();

    ESP_LOGI(TAG, "Initializing ADF periph set");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    set = esp_periph_set_init(&periph_cfg);

    ESP_LOGI(TAG, "Creating I2C mutex");

    i2c_semaphore = (SemaphoreHandle_t *)malloc(sizeof(SemaphoreHandle_t));
    *i2c_semaphore = xSemaphoreCreateMutex();
    if (!i2c_semaphore)
    {
        ESP_LOGE(TAG, "Could not create I2C semaphore");
        abort();
    }

    xSemaphoreGive(*i2c_semaphore);

    qwiictwist_info = qwiictwist_malloc();
    qwiictwist_init(qwiictwist_info, I2C_MASTER_NUM, 0x3F, i2c_semaphore);

    ESP_LOGI(TAG, "Initializing MCP23017 driver");
    mcp23017_info = mcp23017_malloc();
    ESP_ERROR_CHECK(mcp23017_init(mcp23017_info, I2C_MASTER_NUM, I2C_MCP23017_ADDRESS, i2c_semaphore));

    ESP_LOGI(TAG, "Initializing HD47780 driver");
    lcd_info = lcd_malloc();
    ESP_ERROR_CHECK(lcd_init(lcd_info, I2C_MASTER_NUM, I2C_LCD_ADDRESS, i2c_semaphore));

    ESP_LOGI(TAG, "Initializing SD driver");
    audio_board_sdcard_init(set, SD_MODE_1_LINE);
}

static void _init_modules()
{
    ESP_LOGI(TAG, "Initialising inputs");
    init_inputs(set, mcp23017_info, qwiictwist_info);

    ESP_LOGI(TAG, "Initialize ip");
    init_ip_module();
    ESP_LOGI(TAG, "Initialize time");
    init_time_module();
    ESP_LOGI(TAG, "Initialize weather");
    init_weather_module();
    ESP_LOGI(TAG, "Initialize audio");
    init_audio_module(board, set);
    ESP_LOGI(TAG, "Initialize wifi");
    init_wifi();
}

esp_err_t draw_frame(int x, int y, char *string, int length)
{
    esp_err_t error;

    xSemaphoreTake(frame_semaphore, portMAX_DELAY);

    error = lcd_goto(lcd_info, x, y);
    error = lcd_write_characters(lcd_info, string, length);

    xSemaphoreGive(frame_semaphore);

    return error;
}

void setup_components()
{
    ESP_LOGI(TAG, "Initializing nvs flash");
    ESP_ERROR_CHECK(nvs_flash_init());

    ESP_LOGI(TAG, "Initializing default event loop");
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    _init_drivers();
    _init_modules();

    frame_semaphore = xSemaphoreCreateMutex();
    xSemaphoreGive(frame_semaphore);
}