#include <time.h>
#include <freertos/FreeRTOS.h>
#include <assert.h>
#include <string.h>
#include <esp_log.h>

#include <audio_module.h>

#include <core.h>
#include "alarm.h"

static const char TAG[] = "alarm";

alarm_info_t *init_alarm(char *alarm_sound, frame_t *alarm_frame)
{
    alarm_info_t *alarm = (alarm_info_t *)malloc(sizeof(alarm_info_t));
    memset(alarm, 0, sizeof(alarm_info_t));

    alarm->sound = alarm_sound;
    alarm->frame = alarm_frame;
    alarm->time = NULL;

    return alarm;
}

void play_alarm(alarm_info_t *info)
{
    assert(info != NULL);
    assert(info->sound != NULL);

    ESP_LOGD(TAG, "Play alarm");

    play_mp3_continuous(info->sound);
    info->is_playing = true;
}

void stop_alarm(alarm_info_t *info)
{
    if (info->is_playing)
    {
        ESP_LOGD(TAG, "Stop alarm");

        stop_audio();

        remove_alarm(info);
    }
}

void check_alarm(alarm_info_t *info, struct tm *time)
{
    assert(info != NULL);

    ESP_LOGD(TAG, "Checking alarm");

    if (info->time == NULL)
    {
        return;
    }

    if (info->time->tm_hour == time->tm_hour && info->time->tm_min == time->tm_min)
    {
        ESP_LOGD(TAG, "Alarm goes off");

        play_alarm(info);
    }
}

void set_alarm(alarm_info_t *info, struct tm *time)
{
    assert(info != NULL);

    ESP_LOGD(TAG, "Setting alarm");

    if (time == NULL)
    {
        remove_alarm(info);
        return;
    }

    if (info->time == NULL)
    {
        free(info->time);
    }

    info->time = (struct tm *)malloc(sizeof(struct tm));
    *info->time = *time;

    char out[50];
    strftime(out, 100, "%H:%M", time);
    frame_clear(info->frame);
    frame_write_string(info->frame, 0, 0, out);
    frame_flush(info->frame);
}

void remove_alarm(alarm_info_t *info)
{
    assert(info != NULL);

    ESP_LOGD(TAG, "Removing alarm");

    if (info->time != NULL)
    {
        free(info->time);
        info->time = NULL;
    }

    info->is_playing = false;

    frame_clear(info->frame);
    frame_flush(info->frame);
}